﻿using System.Collections;
using UnityEngine;
using UnityEditor;

public class ObjetoController : MonoBehaviour
{
	[SerializeField] private CharacterController controller;
	private Vector3 direction;
	public float forwardSpeed;
	public float maxSpeed;

	private int desiredLane = 1;//0:left, 1:middle, 2:right
	public float laneDistance = 2.5f;//The distance between tow lanes

	//public bool isGrounded;
	public LayerMask groundLayer;
	public Transform groundCheck;

	public float jumpForce;
	public float Gravity = -20;


	private bool isSliding = false;

	void Start()
	{
		controller = GetComponent<CharacterController>();
	}

	void Update()
	{
		if (!ObjetoManager.isGameStarted)
			return;



		if(forwardSpeed < maxSpeed)
			forwardSpeed += 0.1f * Time.deltaTime;


		direction.z = forwardSpeed;
		direction.y +=Gravity*Time.deltaTime;






		//SwipeInputs
		if (SwipeManager.swipeRight)
		{
			desiredLane++;
			if (desiredLane == 3)
				desiredLane = 2;
		}
		if (SwipeManager.swipeLeft)
		{
			desiredLane--;
			if (desiredLane == -1)
				desiredLane = 0;
		}



		//Calculate where we should be in the future
		Vector3 targetPosition = transform.position.z * transform.forward + transform.position.y * transform.up;
		if (desiredLane == 0)
			targetPosition += Vector3.left * laneDistance;
		else if (desiredLane == 2)
			targetPosition += Vector3.right * laneDistance;


		if (transform.position == targetPosition)
			return;
		Vector3 diff = targetPosition - transform.position;
		Vector3 moveDir = diff.normalized * 25 * Time.deltaTime;
		if (moveDir.sqrMagnitude < diff.magnitude)
			controller.Move(moveDir);
		else
			controller.Move(diff);


	}
	private void FixedUpdate()
	{
		if (!ObjetoManager.isGameStarted)
		return;
		controller.Move(direction * Time.fixedDeltaTime);
	}

	private void Jump()
	{
		direction.y = jumpForce;
	}

	private void OnControllerColliderHit(ControllerColliderHit hit)
	{



		if(hit.transform.tag == "obstaculo")
		{
			ObjetoManager.gameOver = true;


		}
	}

	private IEnumerator Slide()
	{
		isSliding = true;

		controller.center = new Vector3(0, -0.5f, 0);
		controller.height = 1;

		yield return new WaitForSeconds(1.3f);

		controller.center = Vector3.zero;
		controller.height = 2;

		isSliding = false;
	}

}
